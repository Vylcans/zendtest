<?php

namespace Admin;

return array(

    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => Controller\IndexController::class,
            'Admin\Controller\Category' => Controller\CategoryController::class,
        ),
    ),

    'router' => array(
        'routes' => array(

            'admin' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'category' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    => 'category/[:action/][:id/]',
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Category',
                                'action' => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(

        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

);
