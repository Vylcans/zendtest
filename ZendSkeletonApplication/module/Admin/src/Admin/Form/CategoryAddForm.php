<?php

namespace Admin\Form;


use Zend\Form\Form;

class CategoryAddForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('categoryAddForm');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'bs-example form-horizontal');

        $this->add([
            'name' => 'categoryKey',
            'type' => 'text',
            'options' => [
                'min' => 3,
                'max' => 100,
                'label' => 'Atslēga',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => 'required',
            ],
        ]);

        $this->add([
            'name' => 'categoryName',
            'type' => 'text',
            'options' => [
                'min' => 3,
                'max' => 100,
                'label' => 'Nosaukums',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => 'required',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Saglabāt',
                'class' => 'btn btn-primary',
                'id' => 'btn-submit',
            ],
        ]);
    }
}