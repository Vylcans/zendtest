<?php

namespace Admin\Controller;

use Application\Controller\BaseController;

class BaseAdminController extends BaseController
{
    public function onDispatch(\Zend\Mvc\MvcEvent $event){
        return parent::onDispatch($event);
    }
}

