<?php

namespace Admin\Controller;

use Admin\Controller\BaseAdminController as BaseController;
use Admin\Form\CategoryAddForm;
use Blog\Entity\Category;
use Doctrine\Common\Collections\Criteria;

class CategoryController extends BaseController
{

    public function indexAction()
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM Blog\Entity\Category c ORDER BY c.id DESC'
            );
        $categories = $query->getResult();

        /*
        $criteria = new Criteria();
        $criteria->andWhere(
            $criteria->expr()->contains('categoryName', 'name')
        );

        $categories = $this->getEntityManager()
            ->getRepository('Blog\Entity\Category')
            ->matching($criteria);
        */

        return [
            'categories' => $categories,
        ];
    }

    public function addAction()
    {
        $form = new CategoryAddForm();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $em = $this->getEntityManager();

            $form->setData($request->getPost());

            $status = 'error';
            $message = 'Kļūda';

            if ($form->isValid()) {

                $category = new Category();
                $category->exchangeArray($form->getData());

                $em->persist($category);
                $em->flush();

                $status = 'success';
                $message = 'Kategorija pievienota';
            }

            if ($message) {
                $this->flashMessenger()
                    ->setNamespace($status)
                    ->addMessage($message);
            }

            return $this->redirect()->toRoute('admin/category');
        }

        return [
            'form' => $form,
        ];
    }

    public function editAction()
    {
        $form = new CategoryAddForm();

        $id = (int) $this->params()->fromRoute('id', 0);

        $em = $this->getEntityManager();
        $category = $em->find('Blog\Entity\Category', $id);

        if (empty($category)) {
            $message = 'Kategorija nav atrasta';
            $status = 'error';
            $this->flashMessenger()
                ->setNamespace($status)
                ->addMessage($message);

            return $this->redirect()->toRoute('admin/category');
        }

        $form->bind($category);
        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            $status = 'error';
            $message = 'Kļūda';

            if ($form->isValid()) {

                $category->exchangeArray($form->getData());

                $em->persist($category);
                $em->flush();

                $status = 'success';
                $message = 'Kategorija labota';

            } else {
                foreach ($form->getInputFilter()->getInvalidInput() as $errors) {
                    foreach ($errors->getMessages() as $error) {
                        $message .= ' ' . $error;
                    }
                }
            }

            $this->flashMessenger()
                ->setNamespace($status)
                ->addMessage($message);

            return $this->redirect()->toRoute('admin/category');
        }

        return [
            'form' => $form,
            'id' => $id,
        ];
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        /**
         * @var Doctrine\Common\Persistence\ObjectManager $em
         */
        $em = $this->getEntityManager();


        /**
         * @var Doctrine\Common\Persistence\ObjectRepository $repository
         */
        $repository = $em->getRepository('Blog\Entity\Category');
        $category = $repository->find($id);

        if (empty($category)) {
            $message = 'Kategorija nav atrasta';
            $status = 'error';
        } else {

            $message = 'Kategorija izdzēsta';
            $status = 'success';

            try {
                $em->remove($category);
                $em->flush();
            } catch(\Exception $e) {
                $message = $e->getMessage();
                $status = 'success';
            }
        }

        $this->flashMessenger()
            ->setNamespace($status)
            ->addMessage($message);

        return $this->redirect()->toRoute('admin/category');
    }
}

